import sys
from ete2 import Tree
from tree_utils import label_all_nodes, assign_node_ids, assign_depth_level, build_id_map, postorder, print_tree
from lca import build_lca_map
from linked_list import LinkedList

def read_input(stree_filename, gtree_filenames):
    try:
        stree = Tree(stree_filename,format=1)
    except:
        print "Malformed newick tree structure in species tree representation."
        quit()
    i = 1
    gtrees = []
    for f in gtree_filenames:
        try:
            gtrees.append(Tree(f,format=1))
        except:
            print "Malformed newick tree structure in %d-th gene tree representation." % i
            quit()
        i += 1

    return stree, gtrees


def initialize_tree(root,name_format):
    label_all_nodes(root,name_format)
    assign_node_ids(root)
    assign_depth_level(root)


def update_gtree_map_height(gtree):

    def update_map_height(node):
        cheights = [c.mheight 
                    for c in node.children
                    if c.maps_to_id == node.maps_to_id]
        if len(cheights)>0:
            max_height = max(cheights)
        else:
            max_height = 0

        node.mheight = 1 + max_height
        node.mroot = ((node.up == None) or
                      (node.up.maps_to_id != node.maps_to_id))

    postorder(gtree,
              update_map_height)


def compute_lca_and_init_all_vars(stree, stree_nodes, gtrees, gtree_nodes):

    for v in stree_nodes.values():
        v.mheight = 0
        v.leading_nodes = LinkedList()
        v.mapped_to = []
        v.step = 0

    for t,tnodes in zip(gtrees,gtree_nodes):
        tmap = build_lca_map(t, stree)

        for uid,vid in tmap.items():
            u = tnodes[uid]
            v = stree_nodes[vid]

            u.maps_to = v
            u.maps_to_id = vid
            
        update_gtree_map_height(t)

        for uid,vid in tmap.items():
            u = tnodes[uid]
            v = stree_nodes[vid]

            if not u.mroot:
                v.mapped_to.append(u)
                continue

            if u.up==None:
                u.step = v.depth
            else:
                u.step = v.depth - stree_nodes[u.up.maps_to_id].depth - 1

            if v.mheight < u.mheight:
                v.mheight = u.mheight
                v.mapped_to += list(v.leading_nodes)
                v.leading_nodes = LinkedList([u])
                v.step = u.step
            elif v.mheight == u.mheight:
                v.leading_nodes.append(u)
                v.step = min(v.step, u.step)


def update_mapping(node):
    for child in node.children:
        update_mapping(child)

    if (node.up==None) or (node.leading_nodes.is_empty()):
        return

    if node.step == 0:
        return

    pu = node.up

    if pu.mheight > 1:
        return
    if pu.mheight == 1:
        pu.leading_nodes.merge(node.leading_nodes)
        pu.step = min(pu.step, node.step-1)
        node.leading_nodes = LinkedList()
    elif pu.mheight == 0:
        pu.mheight = 1
        pu.leading_nodes = node.leading_nodes
        pu.step = node.step-1
        node.leading_nodes = LinkedList()
        

def finalize_mapping(node):
    for child in node.children:
        finalize_mapping(child)
    
    for u in node.mapped_to:
        u.maps_to = node
        u.maps_to_id = node.id

    for u in node.leading_nodes:
        u.maps_to = node
        u.maps_to_id = node.id

    node.mapped_to += node.leading_nodes
    node.leading_nodes = LinkedList()


def compute_cost(stree, gtrees):

    def reset_mheight(snode):
        snode.mheight = 0

    def update_snode_mheight(gnode):
        if gnode.maps_to.mheight < gnode.mheight:
            gnode.maps_to.mheight = gnode.mheight

    def update_cost(snode, cost):
        cost[0] += snode.mheight

    postorder(stree,
              lambda n: reset_mheight(n))

    for gt in gtrees:
        update_gtree_map_height(gt)
        
        postorder(gt,update_snode_mheight)

    cost = [0]
    postorder(stree,
              lambda n: update_cost(n, cost))

    return cost[0]

def print_snode(n,depth):
    print n.name, 
    print 'M=(' + ','.join([l.name for l in n.mapped_to]) + ')',
    print 'LN=(' + ','.join([l.name for l in n.leading_nodes]) + ')',
    print 'H =',n.mheight,' Step =', n.step

def print_gnode(n,depth):
    print "  %s%s -> %s" % (" "*depth,n.name, n.maps_to.name)

def read_gene_names(file_name):
    return [s.strip() for s in open(file_name).readlines()]

def extract_options(argv):
    options = {}
    if '-gnames' in argv:
        i = argv.index('-gnames')
        if i!=len(argv)-1:
            options['gnames'] = argv[i+1]
            del argv[i+1]
        del argv[i]
    return options


def print_report(stree, gtrees, gnames):
    print "Species trees (where internal nodes are all labelled)"
    print "-----------------------------------------------------"
    print stree.get_ascii()
    print "-----------------------------------------------------"

    print "Results"
    print "======="
    print "Number of episodes: %s" % (compute_cost(stree,gtrees))

    for t,name in zip(gtrees, gnames):
        print
        print "--- %s ---" % name
        print t.get_ascii()
        print
        print "Mapping:"
        print
        print_tree(t, print_gnode)


def main():
    if len(sys.argv) < 3:
        print 'Usage: mdp.py [species tree] [gene tree 1] [gene tree 2] ...'
        quit()

    options = extract_options(sys.argv)

    stree_filename = sys.argv[1]
    gtree_filenames = sys.argv[2:]

    stree, gtrees = read_input(stree_filename, gtree_filenames)

    initialize_tree(stree,'S%d')
    #print_tree(stree)

    stree_nodes = build_id_map(stree)
    gtree_nodes = []
    c = 1
    for t in gtrees:
        initialize_tree(t,'G' + str(c) + 'n%d')
        nodes = build_id_map(t)
        gtree_nodes.append(nodes)
        c += 1


    compute_lca_and_init_all_vars(stree, stree_nodes,
                                  gtrees, gtree_nodes)

    update_mapping(stree)
    finalize_mapping(stree)

    if 'gnames' in options:
        gnames = read_gene_names(options['gnames'])
    else:
        gnames = gtree_filenames

    print_report(stree,gtrees,gnames)

if __name__=='__main__':
    main()
