def postorder(node,f=None):
    for c in node.children:
        postorder(c,f)
    if f!=None:
        f(node)


def collect_all_node_names(root):
    names = set()
    postorder(root, lambda n: names.add(n.name))
    if 'NoName' in names:
        names.remove('NoName')
    return names


def label_all_nodes(root, name_format='Node%d'):
    def assign_name(node, used_names, counter):
        if node.name=='NoName':
            c = counter['counter']
            while (name_format % c) in used_names:
                c += 1
            node.name = name_format % c
            used_names.add(name_format % c)
            counter['counter'] = c

    names = collect_all_node_names(root)
    counter = {'counter':0}
    postorder(root,
              lambda n: assign_name(n, names, counter))


def assign_depth_level(root, depth=0):
    root.depth = depth
    for child in root.children:
        assign_depth_level(child, depth+1)


def assign_node_ids(root,nid=0):
    root.id=nid
    for c in root.children:
        nid = assign_node_ids(c,nid+1)
    return nid
    

def build_map(root, node_to_key):
    def assign_node(nodes,k,node):
        nodes[k] = node
    nodes = {}
    postorder(root,
              lambda n: assign_node(nodes,node_to_key(n),n))
    return nodes

def build_id_map(root):
    return build_map(root,
                     lambda n: n.id)

def build_name_map(root):
    return build_map(root,
                     lambda n: n.name)


def print_tree(root,print_function=None,depth=0):
    if print_function==None:
        print ('  ' * depth), root.name
    else:
        print_function(root,depth)
    for c in root.children:
        print_tree(c,print_function,depth+1)

