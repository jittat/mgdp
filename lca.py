from tree_utils import build_name_map, print_tree, postorder
from rmq import RMQ

def build_rmq_array(stree):
    def update_array(node,array,first,last):
        l = len(array)
        array.append(node.id)
        if node.id not in first:
            first[node.id] = l
        last[node.id] = l
        #print ">",node.id,l

    def inorder(root,array,first,last):
        ccount = len(root.children)
        if ccount==0:
            update_array(root, array, first, last)
            
        c = 0
        for child in root.children:
            inorder(child,array,first,last)
            if (c==0) or (c!=ccount-1):
                update_array(root, array, first, last)
            c+=1

    array = []
    first = {}
    last = {}
    inorder(stree, array, first, last)
    return array, first, last

def build_lca_map(gtree, stree):
    def update_map(node, gmap, snode_names, r, f, l):
        if node.is_leaf():
            gmap[node.id] = snode_names[node.name].id
            #print "m:", node.id, node.name, snode_names[node.name].name
        else:
            a = gmap[node.children[0].id]
            #print "mm:", node.id,a,
            for c in node.children[1:]:
                b = gmap[c.id]
                #print b,
                if l[b]<f[a]:
                    a,b = b,a
                a = r.query(f[a],l[b])
            gmap[node.id] = a
            #print

    #print_tree(stree)
    a, first, last = build_rmq_array(stree)
    snode_names = build_name_map(stree)
    r = RMQ(a)
    gmap = {}
    postorder(gtree,
              lambda n: update_map(n, gmap, snode_names,
                                   r, first, last))
    return gmap

