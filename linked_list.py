class LinkedList:

    class Node:
        def __init__(self, val, next=None):
            self.val = val
            self.next = next

    def append(self,val):
        n = LinkedList.Node(val)
        self.tail.next = n
        self.tail = n

    def __init__(self, ls=None):
        dummy = LinkedList.Node(None)
        self.head = dummy
        self.tail = dummy
        if ls != None:
            for x in ls:
                self.append(x)

    def is_empty(self):
        return id(self.head)==id(self.tail)

    def merge(self, ls):
        if ls.is_empty():
            return

        self.tail.next = ls.head.next
        self.tail = ls.tail

        return self

    def __iter__(self):
        p = self.head.next
        while p!=None:
            yield p.val
            p = p.next
        raise StopIteration()


if __name__=='__main__':
    l = LinkedList([1,2,3,4])
    print list(l)
    l.append(100)
    print list(l)
    l.merge(LinkedList([10,20,30,40]))
    print list(l)

