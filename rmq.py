from random import randint

class RMQ:
    @staticmethod
    def log2f(n):
        r = 0
        l = 1
        while n>=l:
            r += 1
            l *= 2
        return r-1

    @staticmethod
    def log2c(n):
        r = 0
        l = 1
        while n>l:
            r += 1
            l *= 2
        return r

    def get_array(self,l,i):
        if i>=self.n:
            return self.infinity
        else:
            return self.s[l][i]

    def preprocess(self):
        # first level
        for x in self.a:
            self.s[0].append(x)
        for l in range(1,self.k):
            r = 2 ** (l-1)
            for i in range(self.n):
                self.s[l].append(min(self.s[l-1][i],
                                     self.get_array(l-1,i+r)))

    def __init__(self,a):
        self.a = a
        self.n = len(a)
        self.k = RMQ.log2c(self.n)+1
        self.s = [[] for l in range(self.k)]
        self.infinity = max(a)+10
        self.preprocess()

    def query(self,i,j):
        l = RMQ.log2f(j-i+1)
        return min(self.s[l][i],
                   self.s[l][j-(2**(l))+1])


def test_rqm():
    n = 1000
    a = [randint(1,10000) for i in range(n)]
    rmq = RMQ(a)
    for i in range(n):
        for j in range(n-i):
            q = rmq.query(i,i+j)
            if min(a[i:i+j+1])!=q: 
                print "Error at",i,j
                quit()
    print "Correct."


if __name__=='__main__':
    test_rqm()
